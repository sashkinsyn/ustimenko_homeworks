// 1. Разобраться, что выводит код и написать объяснение в комментарий

let myVar; //Объявляется перменная(с блочной областью видимости) с именем myVar
myVar = 5; //Объявленной переменной myVar присваивается численное значение 5
let myNum; //Объявляется перменная(с блочной областью видимости) с именем myNum
myNum = myVar; //Объявленной переменной myNum присваивается значение пременной myVar, то есть значение равно 5
myVar = 10; //Переменной myVar присваевается новое численное значение 10
myNum = myVar; //Переменной myNum присваивается новое значение пременной myVar, то есть значение равно 10
// код выводит 10


// 2. Разобраться, что выводит код и написать объяснение в комментарий
const myVar; //Скрипт на этой же строке выдаст синтаксическую ошибку. Нельзя объявлять константу не присвоив ей значение (не инициализировать).
myVar = 5; // Если бы не было ошибки выше, то ошибка была бы здесь.Нельзя константам присваивать другие значения нужно было объединить первую и вторую строку (const myVar = 5;)
const myNum; // Такая же ошибка как и в первой строке
myNum = myVar; //Опять же нельзя так писать нужно было объединить строки (const myNum = myVar;) Но при условии что до этого константа myVar была объявленна верно
myVar = 10; // При условии что ошибок выше не было бы, то ошибка появилась бы тут - константам нельзя изменять значения.
myNum = myVar; // При условии что ошибок выше не было бы, то ошибка появилась бы тут - константам нельзя изменять значения.
// код выводит Uncaught SyntaxError: Missing initializer in const declaration


// 3. Разобраться, что выводит код и написать объяснение в комментарий
let a = 123 // Объявляется переменная a с числовым значением 123
let b = -123; // Объявляется переменная b с числовым значением -123
let c = "Hello"; //Объявляется переменная с со строковым значением 'Hello'
const e = 123; // Объявляется константа e с числовым значением 123

a = a + 1; //Переменной a присваивается новое значение результат суммы значения a и 1 (теперь переменная a = 124;)
b = +5; //Переменной b присваивается новое значение +5 (теперь переменная b = 5;)
c = c + " world!" //Переменной с присваивается новое значение результат конкатенации строк (теперь переменная с = 'Hello world!';)
e = e + 123; //Ошибка, так как нельзя присваивать константе новое значение.
// код выводит Uncaught TypeError: Assignment to constant variable.



// 4. Разобраться, что выводит код и написать объяснение в комментарий

let a = 0; // Объявляется переменная a с числовым значением 0
let b; // Объявляется переменная b 

if (a) {
    console.log(a);
} else if (b) {
    console.log(b);
} else {
    console.log('hello world');
}
/*} Конструкция условного оператора if выполняется в том случае если условие (то что в скобках) после приведения к булевому значению == true.
Так как число 0 приводится к false, значит метод console.log(a) не выполнится.
Так как переменная b не инициализированна при объявлении, то b имеет значение undefined. undefined приводится к false, значит метод console.log(b) не выполнится.
Так как не одно из условий if не выполнятеся, то выполнится код указанный в блоке else,  значит выполнится метод console.log('hello world')
*/



// 5. Необходимо проверить на тип переменную a. Посмотреть, что выводит код и написать объяснение в комментарий

let a = "Hello world";

if (typeof(a) === "string") {
    console.log(a);
}
/* в первой строке  объявляется переменная a со строковым значением "Hello world"
Далее у нас идёт условие  в которм мы заменили ? ? ? ? ? на typeof(a). Оператор typeof возвращает строку указывающий тип операнда.
В условии написанно строгое сравнение так как typeof(a) вернёт "string" то результатом строгого сравнения будет true ("string" === "string"), а значит код в конструкции if выполнится,
а после выполнения метода console.log(a) в консоль выведется надпись  Hello world.
*/


// 6. Разобраться, что выводит код и написать объяснение в комментарий
let a = {};
let b = a;

alert(a == b);
alert(a === b);

/* В первой строке объявляется пустой объект и переменная a ссылается на этот пустой объект
Во второй строке объявляется переменная b и она ссылается на тот же объект что и a. Объекты это не примитивные типы данных хранятся и копируются они по ссылке.
То есть a и b ссылаются на один и тот же объект.
далее оператор alert выведет нам диалоговые окна по очереди, в каждом из которых будет выведенно true
при строгом и нестрогом сравнении одного и того же объекта будет выведенно true.
*/


// Результаты необходимо залить в репозиторий в ветку feature/hwjs_05 и отправить на review ментору